# RolePacotes

Role de exemplo para instalação de pacotes e repositórios.
Proposito é demonstrar duas coisas: 
1) versionamento da role no git pra uso com ansible-galaxy e 
2) fazer a mesma coisa de duas maneiras diferentes para demonstrar migração parcial de scripts / shell para módulos


Esta role recebe como parametros uma estrutura:
- Uma lista de repositorios.
Dentro desta lista, temos as informações necessárias para adicionar repositorios apt
Chave GPG, string do repositorio

- uma lista de pacotes apt para serem instalados

ex:

```
    packages:
      repositorios:
        - name: hashicorp
          apt_key_url: https://apt.releases.hashicorp.com/gpg
          repo: "deb [signed-by=/etc/apt/keyrings/hashicorp-keyring.asc] https://apt.releases.hashicorp.com {{ ansible_facts['lsb']['codename'] }} main"

        - name: ErlangCloudsmith
          apt_key_url: https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/gpg.E495BB49CC4BBE5B.key
          repo: "deb [signed-by=/etc/apt/keyrings/ErlangCloudsmith-keyring.asc] https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/deb/ubuntu {{ ansible_facts['lsb']['codename']  }} main"

        - name: RabbitMQServer
          apt_key_url: https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-server/gpg.9F4587F226208342.key
          repo: "deb [signed-by=/etc/apt/keyrings/RabbitMQServer-keyring.asc] https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-server/deb/ubuntu {{ ansible_facts['lsb']['codename']  }} main"
      pacotes:
        - vault
        - erlang-base
        - erlang-asn1
```
A versão 1.0.0 

Com propósito didático, a versão 1.0.0 baixa a chave, cria o repositorio usando o modulo shell, instala cada pacote da lista também usando o módulo do shell.

O processo de instalar um pacote com shell parece ser trivial mas, se pensarmos na opreração contrária ( remover um pacote), precisariamos escrever mais tasks e cuidar de mais controles.

Na versão 2, vamos substituir a lista simples de pacotes para instalar pelo modulo apt onde podemos, em uma só tarefa especificar se aquele pacote deve estar presente ou ausente (removido do sistema).


A versão 1.1.0

Na versão 1.1.0 , o download e a instalação de chaves será substituida pelo módulo de repositório

Além disso, o módulo de pacote vai fazer o trabalho de três tarefas separadas na versão 1.0